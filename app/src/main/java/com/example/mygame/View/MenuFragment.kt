package com.example.mygame.View

import android.app.AlertDialog
import android.media.MediaPlayer
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.mygame.R
import com.example.mygame.databinding.FragmentMenuBinding


class MenuFragment : Fragment() {
    lateinit var binding: FragmentMenuBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMenuBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val musicMenu = MediaPlayer.create(requireContext(), R.raw.musicmenu)
        musicMenu.setVolume(0.3f, 0.3f)
        musicMenu.isLooping = true
        musicMenu.start()
        val soundButton = MediaPlayer.create(requireContext(), R.raw.soundbutton)

        binding.buttonPlay.setOnClickListener {
            soundButton.start()
            findNavController().navigate(R.id.action_menuFragment_to_gameFragment)
            musicMenu.stop()

        }
        binding.buttonHelp.setOnClickListener {
            soundButton.start()
            val alertDialog = AlertDialog.Builder(requireContext()).create()
            alertDialog.setTitle("COMO SE JUEGA?")
            alertDialog.setMessage("JUEGO DE SUPERVIVENCIA\n" +
                    "\nPilota una nave espacial por toda la galaxia y acaba y elimina disparando a todas las naves enemigas, mientras mas mejor, dispara sin piedad y salva tu vida defendiéndote de todos los ataques.")
            alertDialog.setButton(
                AlertDialog.BUTTON_NEUTRAL, "SALIR"
            ) { dialog, _ -> dialog.dismiss() }
            alertDialog.show()
        }
    }

}







