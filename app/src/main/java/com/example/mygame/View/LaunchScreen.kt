package com.example.mygame.View

import android.annotation.SuppressLint
import android.media.MediaPlayer
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.mygame.R


@SuppressLint("CustomSplashScreen")
class LaunchScreen : Fragment() {
    private lateinit var mediaPlayer: MediaPlayer

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_launch_screen, container, false)

        Handler(Looper.myLooper()!!).postDelayed({
            mediaPlayer.stop()
            findNavController().navigate(R.id.action_launchScreen_to_menuFragment)
        }, 4000)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mediaPlayer = MediaPlayer.create(requireContext(), R.raw.soundalien)
        mediaPlayer.start()

    }

}