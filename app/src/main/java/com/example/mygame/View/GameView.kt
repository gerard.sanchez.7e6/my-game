package com.example.mygame.View

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.media.MediaPlayer
import android.view.MotionEvent
import android.view.SurfaceView
import androidx.navigation.findNavController
import com.example.mygame.R
import kotlinx.coroutines.*

@SuppressLint("ViewConstructor")
class GameView(context: Context, val size: Point) : SurfaceView(context) {
    var canvas: Canvas = Canvas()
    val paint: Paint = Paint()
    val player = Player(context,size.x,size.y)
    val screenElements = mutableListOf<ScreenElement>()
    var playing = true
    val backgroundImage: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.fondo)
    var backroundMusic = MediaPlayer.create(context, R.raw.musicgame)
    var shootSound = MediaPlayer.create(context, R.raw.disparonave)
    var alien = MediaPlayer.create(context, R.raw.aliendead)
    var score= 0


    init {
        startGame()
        spawnEnemies()
        shoot()
    }

    fun startGame(){
        CoroutineScope(Dispatchers.Main).launch{
            while(playing){
                backroundMusic.start()
                backroundMusic.setVolume(0.3f, 0.3f)
                backroundMusic.isLooping = true
                draw()
                update()
                delay(10)
            }
            backroundMusic.stop()
            val action = GameFragmentDirections.actionGameFragmentToResultFragment(score)
            findNavController().navigate(action)
        }
    }

    fun spawnEnemies() {
        GlobalScope.launch {
            while (playing) {
                val enemy = Enemy(context,size.x,size.y)
                screenElements.add(enemy)
                delay(500)

            }
        }
    }

    fun shoot(){
        GlobalScope.launch {
            while (playing) {
                shootSound.start()
                screenElements.add(Bullet(context,size.x,size.y,player.positionX+(player.width/2).toInt(),player.positionY))
                delay(750)
            }
        }
    }


    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event != null) {
            player.positionX = event.x.toInt()-(player.width/2).toInt()
            player.positionY = event.y.toInt()-(player.height/2).toInt()
            player.hitbox.top = player.positionY.toFloat()
            player.hitbox.bottom = player.hitbox.top+player.height
            player.hitbox.left = player.positionX.toFloat()
            player.hitbox.right = player.hitbox.left+player.width
        }
        return true
    }

    fun draw(){
        if (holder.surface.isValid) {
            canvas = holder.lockCanvas()

            paint.color = Color.YELLOW
            paint.textSize = 60f
            paint.textAlign = Paint.Align.RIGHT
            canvas.drawBitmap(backgroundImage, 0f, 0f, null)
            canvas.drawText("Score: $score", (size.x - paint.descent()), 75f, paint)
            canvas.drawBitmap(player.bitmap,player.positionX.toFloat(),player.positionY.toFloat(),paint)
            try{
                for (element in screenElements){
                    if(element is Enemy){
                        var i = 0
                        var dead = false
                        while (i in screenElements.indices&&!dead){
                            if(screenElements[i] is Bullet && RectF.intersects(element.hitbox,screenElements[i].hitbox)){
                                screenElements.remove(element)
                                alien.start()
                                dead = true
                            } else {
                                canvas.drawBitmap(element.bitmap,element.positionX.toFloat(),element.positionY.toFloat(),paint)
                            }
                            i++
                        }
                        if(RectF.intersects(element.hitbox,player.hitbox)) playing=false
                    } else if(element is Bullet){
                        canvas.drawBitmap(element.bitmap,element.positionX.toFloat(),element.positionY.toFloat(),paint)
                    } else {
                        canvas.drawBitmap(element.bitmap,element.positionX.toFloat(),element.positionY.toFloat(),paint)
                        if(RectF.intersects(element.hitbox,player.hitbox)) {
                            screenElements.remove(element)

                        }
                    }
                }
            }catch (_:java.util.ConcurrentModificationException){
            }catch (_:java.lang.IndexOutOfBoundsException){}
            holder.unlockCanvasAndPost(canvas)
        }
    }

    fun update(){
        try{
            score += 1
            for(element in screenElements){
                if(element is Enemy){
                    if(element.positionY<size.y) element.updateElement()
                    else if(element.positionY<=0){
                        screenElements.remove(element)
                    }
                    else {
                        playing = false
                    }
                } else if(element is Bullet) {
                    if(element.positionY>0) element.updateElement()
                    else screenElements.remove(element)
                }
            }
        }catch (_:java.util.ConcurrentModificationException){}
    }

}