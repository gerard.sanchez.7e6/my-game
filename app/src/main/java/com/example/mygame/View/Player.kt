package com.example.mygame.View

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.RectF
import com.example.mygame.R

class Player(context: Context,screenX: Int,screenY: Int) {
    var bitmap: Bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.naveespacial)
    val width = 150
    val height = 200
    var positionX = screenX/2
    var positionY = screenY*2/3
    var hitbox = RectF()

    init{
        bitmap = Bitmap.createScaledBitmap(bitmap, width, height,false)
    }
}
